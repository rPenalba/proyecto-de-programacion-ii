/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Orden;
import model.ProductTableModelList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import view.PnlVentas;


public class VentasController extends KeyAdapter implements ActionListener {

    private PnlVentas list;
    private JFileChooser dialog;

    
     
    public VentasController(PnlVentas list) {
        dialog = new JFileChooser();
        this.list = list;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
           
            case "open":
                showProductsOnTable();
                list.setTotalQuantityAndPurchases();
                break;
            case "newRow":
                list.addRow();
                break;
            case "saveTableContent":
                saveTableContent();
                break;
            case "deleteRow":
                list.removeRow();
                list.setTotalQuantityAndPurchases();
                break;
            case "show":
                list.showListOnTable1();
                list.setTotalQuantityAndPurchases();
                break;
            case "update":
                break;          
          
        }
    }

 

    private void writeFile(File file, Orden product) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(product);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Orden readFile(File file) {
        Orden product = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            product = (Orden) in.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    private Orden[] readProductList(File file) {
        Orden products[] = null;
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(file))) {
            products = (Orden[]) input.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }

    /**
     *  CAMBIO DE MODELO EN LA LINEA 145
     */
    private void showProductsOnTable() {
        dialog.showOpenDialog(list);
        File file = dialog.getSelectedFile();
        if (file != null) {
            //list.setProductTableModel(new ProductTableModel(readProductList(file)));
            list.setProductTableModel(new ProductTableModelList(readProductList(file)));
        }
    }

    private void saveTableContent() {
        dialog.showSaveDialog(list);/*
        try (ObjectOutputStream output
                = new ObjectOutputStream(new FileOutputStream(dialog.getSelectedFile()))) {
            output.writeObject(products);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VentasController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    @Override
    public void keyTyped(KeyEvent e) {
        JTextField input = (JTextField) e.getSource();
        switch (input.getName()) {
            case "price":
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE 
                        || c == KeyEvent.VK_ENTER || c == KeyEvent.VK_PERIOD)) {
                    e.consume();
                }
                break;
            case "productName":
                if (input.getText().length() >= 10) {
                    e.consume();
                }
                break;
                
            case "personName":
                if (input.getText().length() >= 10) {
                    e.consume();
                }
                break;
        }
    }
}
