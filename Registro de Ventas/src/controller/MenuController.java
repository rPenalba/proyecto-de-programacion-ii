
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.PnlMenu;
import view.PnlPlantilla;

public class MenuController implements ActionListener{
    PnlMenu menu;
    MainController mainController;
    
    public MenuController(PnlMenu menu, MainController mainController) {
        this.menu = menu;
        this.mainController = mainController;
    }


    @Override
    public void actionPerformed(ActionEvent evt) {
        javax.swing.JButton btn = (javax.swing.JButton) evt.getSource();
        PnlPlantilla contenedor = (PnlPlantilla)btn.getParent();
        
        menu.addPlatilloOrden(contenedor.getPlatillo());
        mainController.actualizarFrame();
        
        /*
        switch (evt.getActionCommand()) {
            case "AgregarPlatillo" :
                //menu.add(menu);
                break;
        }*/
    }
    
}
