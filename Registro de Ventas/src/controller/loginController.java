/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.VentanaLogin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import view.MainFrame;

/**
 *
 * @author HP
 */
public class loginController implements ActionListener{
    VentanaLogin ventanaL;

    public loginController(VentanaLogin vl) {
        super();
        ventanaL = vl;
    }   

    @Override
    public void actionPerformed(ActionEvent e) {
            switch(e.getActionCommand()){
                case "iniciarSesion":
                    ventanaL.iniciarSesion();
                    ventanaL.dispose();
                    new MainFrame().setVisible(true);
                    break;
                case "Salir":
                    System.exit(0);
                    break;
            }
    }
    
   
    
    
}
