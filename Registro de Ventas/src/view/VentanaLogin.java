/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.loginController;
import javax.swing.JOptionPane;

/**
 *
 * @author Kenny
 */
public class VentanaLogin extends javax.swing.JFrame {
    loginController lc;
    /**
     * Creates new form Ventana
     */
    public VentanaLogin() {
        initComponents();
        setLocationRelativeTo(null);
        setController();    }

    public void setController(){
        lc = new loginController(this);
        this.botonIniciarSesion.addActionListener(lc);
        this.botonSalir.addActionListener(lc);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        botonSalir = new javax.swing.JButton();
        imagenUsuario = new javax.swing.JLabel();
        etiquetaUsuario = new javax.swing.JLabel();
        etiquetaContraseña = new javax.swing.JLabel();
        cajaUsuario = new javax.swing.JTextField();
        botonIniciarSesion = new javax.swing.JButton();
        cajaContraseña = new javax.swing.JPasswordField();
        imagenFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(500, 520));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(201, 233, 252));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204), 2));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        botonSalir.setBackground(new java.awt.Color(255, 82, 27));
        botonSalir.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        botonSalir.setForeground(new java.awt.Color(255, 255, 255));
        botonSalir.setText("Salir");
        botonSalir.setContentAreaFilled(false);
        botonSalir.setFocusPainted(false);
        botonSalir.setOpaque(true);
        jPanel1.add(botonSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 10, 70, 30));

        imagenUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo.png"))); // NOI18N
        jPanel1.add(imagenUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, -1, -1));

        etiquetaUsuario.setBackground(new java.awt.Color(255, 82, 27));
        etiquetaUsuario.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        etiquetaUsuario.setForeground(new java.awt.Color(255, 82, 27));
        etiquetaUsuario.setText("Usuario:");
        jPanel1.add(etiquetaUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 310, 100, 40));

        etiquetaContraseña.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        etiquetaContraseña.setForeground(new java.awt.Color(255, 82, 27));
        etiquetaContraseña.setText("Contraseña:");
        jPanel1.add(etiquetaContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, -1, -1));

        cajaUsuario.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(cajaUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 320, 230, 30));

        botonIniciarSesion.setBackground(new java.awt.Color(255, 82, 27));
        botonIniciarSesion.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        botonIniciarSesion.setForeground(new java.awt.Color(255, 255, 255));
        botonIniciarSesion.setText("Ingresar");
        botonIniciarSesion.setActionCommand("iniciarSesion");
        botonIniciarSesion.setContentAreaFilled(false);
        botonIniciarSesion.setFocusPainted(false);
        botonIniciarSesion.setOpaque(true);
        jPanel1.add(botonIniciarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 450, 120, 40));

        cajaContraseña.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(cajaContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 370, 230, 30));
        jPanel1.add(imagenFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void iniciarSesion(){
        String usuarioCorrecto = "admin";
        String contraseñaCorrecta = "12345";
        
        if(usuarioCorrecto.equals(cajaUsuario.getText())){
            String contraseña = "";
            
            for(int i=0;i<cajaContraseña.getPassword().length;i++){
                contraseña += cajaContraseña.getPassword()[i];
            }
            
            if(contraseñaCorrecta.equals(contraseña)){
                JOptionPane.showMessageDialog(null,"Ha ingresado al sistema");
            }
            else{
                JOptionPane.showMessageDialog(null,"Error, contraseña incorrecta");
                System.exit(0);
            }
        }
        else{
            JOptionPane.showMessageDialog(null,"Error, usuario desconocido");
            System.exit(0);
        }
        
    }
    
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonIniciarSesion;
    private javax.swing.JButton botonSalir;
    private javax.swing.JPasswordField cajaContraseña;
    private javax.swing.JTextField cajaUsuario;
    private javax.swing.JLabel etiquetaContraseña;
    private javax.swing.JLabel etiquetaUsuario;
    private javax.swing.JLabel imagenFondo;
    private javax.swing.JLabel imagenUsuario;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
