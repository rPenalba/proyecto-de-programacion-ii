package view;

import model.Platillo;

public class PnlPlatosOrden extends javax.swing.JPanel {
    private Platillo platillo;
    private static int id;
    private int numPanel = 0;
    
    public PnlPlatosOrden(Platillo platillo) {
        this.platillo = platillo;
        initComponents();
        
        id = numPanel;
        numPanel++;
        
        setDatos();
    }

    public void setDatos() {
        txtNombrePlatillo.setText(platillo.getNombrePlatillo());
    }
    
    public int getID() {
        return id;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel1 = new javax.swing.JLabel();
        txtNombrePlatillo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        spnrCantidad = new javax.swing.JSpinner();
        jCheckBox1 = new javax.swing.JCheckBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(300, 100));
        setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel1.setText("Cantidad:");
        jLabel1.setPreferredSize(new java.awt.Dimension(75, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
        add(jLabel1, gridBagConstraints);

        txtNombrePlatillo.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtNombrePlatillo.setMargin(new java.awt.Insets(0, 5, 0, 0));
        txtNombrePlatillo.setPreferredSize(new java.awt.Dimension(150, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        add(txtNombrePlatillo, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jLabel2.setText("Nombre: ");
        jLabel2.setPreferredSize(new java.awt.Dimension(75, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
        add(jLabel2, gridBagConstraints);

        spnrCantidad.setModel(new javax.swing.SpinnerNumberModel((byte)0, null, null, (byte)1));
        spnrCantidad.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
        add(spnrCantidad, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        add(jCheckBox1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSpinner spnrCantidad;
    private javax.swing.JTextField txtNombrePlatillo;
    // End of variables declaration//GEN-END:variables
}
