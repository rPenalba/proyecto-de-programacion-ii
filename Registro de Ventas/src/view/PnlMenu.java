
package view;

import controller.MainController;
import controller.MenuController;
import java.util.ArrayList;
import model.Menu;
import model.Platillo;

public class PnlMenu extends javax.swing.JPanel {
    ArrayList<Platillo> listaPlatillos;
    ArrayList<PnlPlantilla> plantillasPlatos;
    ArrayList<PnlPlatosOrden> platosOrden;
    MenuController menuC;
    MainController mainC;

    public PnlMenu(MainController mainC) {
        listaPlatillos = (new Menu()).getPlatillos();
        plantillasPlatos = new ArrayList<PnlPlantilla>();
        platosOrden = new ArrayList<PnlPlatosOrden>();
        menuC = new MenuController(this, mainC);
        
        initComponents();
             
        addPaneles();
    }
    
    public void addPaneles() {
        for (int i = 0; i < listaPlatillos.size(); i++) {
            plantillasPlatos.add(new PnlPlantilla(listaPlatillos.get(i), menuC));
            pnlContenedor.add(plantillasPlatos.get(i));
            
        }
        pnlContenedor.setSize(pnlContenedor.getLayout().preferredLayoutSize(pnlContenedor));
        
        plantillasPlatos.trimToSize();
    }
    
    public void addPlatilloOrden(Platillo platillo) {
        platosOrden.add(new PnlPlatosOrden(platillo));
        pnlPlatillos.add(platosOrden.get(0));
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlCabezera = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        scrollMenu = new javax.swing.JScrollPane();
        pnlContenedor = new javax.swing.JPanel();
        pnlOrden = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        pnlPlatillos = new javax.swing.JPanel();

        setForeground(new java.awt.Color(102, 102, 102));
        setPreferredSize(new java.awt.Dimension(1200, 800));
        setLayout(new java.awt.BorderLayout());

        pnlCabezera.setBackground(new java.awt.Color(255, 82, 27));
        pnlCabezera.setPreferredSize(new java.awt.Dimension(1200, 75));
        pnlCabezera.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 20)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("MENÚ");
        jLabel1.setPreferredSize(new java.awt.Dimension(100, 50));
        pnlCabezera.add(jLabel1, new java.awt.GridBagConstraints());

        add(pnlCabezera, java.awt.BorderLayout.PAGE_START);

        scrollMenu.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollMenu.setPreferredSize(new java.awt.Dimension(900, 725));

        pnlContenedor.setBackground(new java.awt.Color(201, 233, 252));
        pnlContenedor.setPreferredSize(new java.awt.Dimension(1200, 2300));
        pnlContenedor.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 50, 50));
        scrollMenu.setViewportView(pnlContenedor);

        add(scrollMenu, java.awt.BorderLayout.CENTER);

        pnlOrden.setPreferredSize(new java.awt.Dimension(350, 725));
        pnlOrden.setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(201, 233, 252));
        jPanel1.setPreferredSize(new java.awt.Dimension(10, 50));
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jButton1.setBackground(new java.awt.Color(255, 82, 27));
        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Nueva Orden");
        jButton1.setContentAreaFilled(false);
        jButton1.setOpaque(true);
        jButton1.setPreferredSize(new java.awt.Dimension(150, 40));
        jPanel1.add(jButton1);

        jButton2.setText("Guardar");
        jButton2.setPreferredSize(new java.awt.Dimension(73, 40));
        jPanel1.add(jButton2);

        jButton3.setPreferredSize(new java.awt.Dimension(73, 40));
        jPanel1.add(jButton3);

        pnlOrden.add(jPanel1, java.awt.BorderLayout.NORTH);

        jPanel2.setBackground(new java.awt.Color(255, 102, 0));
        jPanel2.setLayout(new java.awt.BorderLayout());

        pnlPlatillos.setPreferredSize(new java.awt.Dimension(350, 10));
        jScrollPane1.setViewportView(pnlPlatillos);

        jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pnlOrden.add(jPanel2, java.awt.BorderLayout.CENTER);

        add(pnlOrden, java.awt.BorderLayout.EAST);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlCabezera;
    private javax.swing.JPanel pnlContenedor;
    private javax.swing.JPanel pnlOrden;
    private javax.swing.JPanel pnlPlatillos;
    private javax.swing.JScrollPane scrollMenu;
    // End of variables declaration//GEN-END:variables
}
