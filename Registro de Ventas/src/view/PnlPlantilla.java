/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.MenuController;
import controller.VentasController;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import model.Platillo;
import model.Orden;

public class PnlPlantilla extends javax.swing.JPanel {
    private Platillo platillo;
    private MenuController mc;

    public PnlPlantilla(Platillo platillo, MenuController mc) {
        this.platillo = platillo;
        this.mc = mc;
        
        initComponents();
        
        setDatos();
        setUpController();
        
    }
    
    public void setDatos() {
        try {
            lblImage.setIcon(new javax.swing.ImageIcon(getClass().getResource(platillo.getImagenPlatillo())));
        } catch (Exception e) {
            
            lblImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/images/platillos/Hamburguesa Sencilla de Res.jpg")));
        }
        lblNombre.setText(platillo.getNombrePlatillo());
        lblPrecio.setText("C$ "+platillo.getPrecioPlatillo());
    }
    
      private void setUpController() {
        this.btnAgregarPlatillo.addActionListener(mc);
        
    }
      
    public Platillo getPlatillo() {
        return platillo;
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        lblImage = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblPrecio = new javax.swing.JLabel();
        btnAgregarPlatillo = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        setMinimumSize(new java.awt.Dimension(300, 400));
        setPreferredSize(new java.awt.Dimension(300, 400));
        setLayout(new java.awt.GridBagLayout());

        lblImage.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 11, 0);
        add(lblImage, gridBagConstraints);

        lblNombre.setBackground(new java.awt.Color(255, 255, 255));
        lblNombre.setFont(new java.awt.Font("Segoe UI", 0, 20)); // NOI18N
        lblNombre.setForeground(new java.awt.Color(255, 82, 27));
        lblNombre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNombre.setText("Nombre del Producto");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(9, 0, 9, 0);
        add(lblNombre, gridBagConstraints);

        lblPrecio.setBackground(new java.awt.Color(255, 255, 255));
        lblPrecio.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblPrecio.setForeground(new java.awt.Color(255, 82, 27));
        lblPrecio.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPrecio.setLabelFor(lblPrecio);
        lblPrecio.setText("Precio del Producto");
        lblPrecio.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(9, 0, 9, 0);
        add(lblPrecio, gridBagConstraints);

        btnAgregarPlatillo.setBackground(new java.awt.Color(255, 82, 27));
        btnAgregarPlatillo.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnAgregarPlatillo.setForeground(new java.awt.Color(255, 255, 255));
        btnAgregarPlatillo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menu.png"))); // NOI18N
        btnAgregarPlatillo.setActionCommand("AgregarPlatillo");
        btnAgregarPlatillo.setContentAreaFilled(false);
        btnAgregarPlatillo.setFocusPainted(false);
        btnAgregarPlatillo.setOpaque(true);
        btnAgregarPlatillo.setPreferredSize(new java.awt.Dimension(120, 60));
        btnAgregarPlatillo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAgregarPlatillo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarPlatilloActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 12, 0);
        add(btnAgregarPlatillo, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarPlatilloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarPlatilloActionPerformed
       // Operaciones n= new Operaciones(plato);
       // n.setVisible(true);
    }//GEN-LAST:event_btnAgregarPlatilloActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarPlatillo;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblPrecio;
    // End of variables declaration//GEN-END:variables
}
