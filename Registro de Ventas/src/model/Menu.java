
package model;

import java.util.ArrayList;

public class Menu {
    ArrayList<Platillo> platillos = new ArrayList<Platillo>();
    
    public Menu() {
        platillos.add(new Platillo("Hamburguesa Sencilla de Res", (short)120));
        platillos.add(new Platillo("Hamburguesa Doble de Res", (short)140));
        platillos.add(new Platillo("Hamburguesa Triple de Res", (short)160));
        platillos.add(new Platillo("Hamburguesa de Res con Baicon", (short)100));
        platillos.add(new Platillo("Hamburguesa de Pollo", (short)120));
        platillos.add(new Platillo("6 Alitas BBQ y Papas", (short)150));
        platillos.add(new Platillo("12 Alitas BBQ y Papas", (short)260));
        platillos.add(new Platillo("Canasta de Alitas Crispy", (short)570));
        platillos.add(new Platillo("Quesadilla", (short)80));
        platillos.add(new Platillo("Burrito", (short)80));
        platillos.add(new Platillo("Pizza de Jamón", (short)220));
        platillos.add(new Platillo("Quesadilla", (short)80));
        platillos.add(new Platillo("Burrito", (short)80));
        platillos.trimToSize();
    }

    public ArrayList<Platillo> getPlatillos() {
        return platillos;
    }
    
    
}
