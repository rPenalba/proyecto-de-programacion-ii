package model;

import java.nio.file.Path;

public class Platillo {
    String nombrePlatillo ;
    short precioPlatillo;
    String imagenPlatillo = "/resource/images/platillos/";
    
    public Platillo(String nombre, short precio) {
        nombrePlatillo = nombre;
        precioPlatillo = precio;
        imagenPlatillo += nombre + ".jpg";
    }

    public void setNombrePlatillo(String nombrePlatillo) {
        this.nombrePlatillo = nombrePlatillo;
    }

    public void setPrecioPlatillo(short precioPlatillo) {
        this.precioPlatillo = precioPlatillo;
    }

    public void setImagenPlatillo(String imagenPlatillo) {
        this.imagenPlatillo = imagenPlatillo;
    }
    
    
    public String getNombrePlatillo() {
        return nombrePlatillo;
    }

    public short getPrecioPlatillo() {
        return precioPlatillo;
    }

    public String getImagenPlatillo() {
        return imagenPlatillo;
    }
    
}
    
    
