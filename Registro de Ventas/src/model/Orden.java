/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;

public class Orden implements Serializable {
    
    private Platillo[] platillos;
    private byte[] cantidad;
    private double total;
    private Date fecha;
    
    
    public Orden(Platillo[] platillo, byte[] cantidad) {
        platillos = platillo;
        this.cantidad = cantidad;
        
    }

    public Platillo[] getPlatillos() {
        return platillos;
    }

    public byte[] getCantidad() {
        return cantidad;
    }

    public double getTotal() {
        return total;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setPlatillos(Platillo[] platillos) {
        this.platillos = platillos;
    }

    public void setCantidad(byte[] cantidad) {
        this.cantidad = cantidad;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}
